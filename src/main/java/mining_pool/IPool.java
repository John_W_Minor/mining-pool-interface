package mining_pool;

import com.ampex.amperabase.IAddress;
import com.ampex.amperabase.IBlockAPI;
import com.ampex.amperabase.IKiAPI;
import com.ampex.amperabase.data.WLong;
import mining_pool.database.data.IPaymentReceipt;
import mining_pool.database.data.ITimeRange;
import mining_pool.events.IPoolEventHandler;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public interface IPool extends Runnable
{

    IKiAPI getGod();

    void registerEventHandler(IPoolEventHandler _eventHandler);

    void registerEventHandlers(List<IPoolEventHandler> _eventHandlers);

    void updateCurrentHeight(BigInteger _currentLocalHeight);

    void updateCurrentShareDifficulty(BigInteger _currentShareDifficulty);

    void updateCurrentPayPerShare(long _currentPayPerShare);

    ITimeRange getCurrentTimeRange();

    void updateCurrentPayInterval(long _currentPayInterval);

    void addShare(IBlockAPI _block);

    void addPPLNSShare(IBlockAPI _block);

    void endPPLNSRound(List<IBlockAPI> _blocksFoundInRound, double _poolFee, IKiAPI _ki);

    long getN();

    void setN(long _n);

    WLong getCurrentPayPerShare();

    ArrayList<IAddress> getShareVerificationReport();

    IPaymentReceipt popReceipt();

    IPaymentReceipt peekReceipt();

    long getNumberOfLiveReceipts();

    IPaymentReceipt getReceiptByIndex(long _index);

    boolean areReceiptsAvailable();

    long getTotalSharesOfMiner(IAddress _address);

    long getTotalPPLNSSharesOfMiner(IAddress _address);

    WLong getTotalSharesOfCurrentPayPeriod();

    void enableLockDebug();

    void disableLockDebug();

    @Override
    void run();
}
