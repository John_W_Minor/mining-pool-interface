package mining_pool.database.data;

import amp.serialization.IAmpByteSerializable;
import com.ampex.amperabase.IAddress;

public interface IMinerID extends IAmpByteSerializable
{
    String getIDAsHexString();

    String getIDAsString();

    IAddress getIDAsAddress();

    boolean isValidMinerID();

    byte[] serializeToBytes();
}
