package mining_pool.database.data;

import amp.Amplet;
import amp.serialization.IAmpAmpletSerializable;

public interface IPaymentReceipt extends IAmpAmpletSerializable
{
    IMinerID getPayee();

    long getOriginToPay();

    boolean isValidPaymentReceipt();

    Amplet serializeToAmplet();
}
