package mining_pool.database.data;

public interface ITimeRange
{
    long getRange();

    String getRangeAsString();

    long getStartTime();

    boolean isExpired(long _liveInterval);
}
