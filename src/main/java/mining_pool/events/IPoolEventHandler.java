package mining_pool.events;

import mining_pool.IPool;

public interface IPoolEventHandler
{
    void handle(IPool _pool, PoolEventType _type) throws Exception;
}
